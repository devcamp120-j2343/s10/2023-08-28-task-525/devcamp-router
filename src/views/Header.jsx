const Header = () => {
    return (
        <div style={{backgroundColor: "yellow"}}>
            <ul>
                <li><a href="/">Home page</a></li>
                <li><a href="/firstpage">First page</a></li>
                <li><a href="/secondpage/1/2">Second page</a></li>
                <li><a href="/secondpage/1">Second page</a></li>
                <li><a href="/secondpage/2">Second page</a></li>
                <li><a href="/secondpage/1/3">Third page</a></li>
            </ul>
        </div>
    )
}

export default Header;