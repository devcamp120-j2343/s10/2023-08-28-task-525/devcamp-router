import { useEffect } from "react";
import { useParams, useNavigate, useSearchParams } from "react-router-dom";

const SecondPage = () => {
    const navigate = useNavigate();
    const { productId, typeId } = useParams();

    // Lấy dữ liệu từ request query url: http://localhost:3000/secondpage/1/2?size=43&type=3
    const [query] = useSearchParams();
    console.log(query.get("type"));
    console.log(query.get("size"));
    
    // Gọi API từ phía server để lấy thông tin sản phẩm (Sử dụng useEffect)

    useEffect(() => {
        if(typeId === '3') {
            navigate("/thirdpage");
        }
    }, []);

    return (
        <div>
            <p>Second page</p>

            {
                productId ?
                <p>Product ID: {productId}</p>
                : <></>
            }

            {
                typeId ?
                <p>Type ID: {typeId}</p>
                : <></>
            }


        </div>
    )
}

export default SecondPage;